export class Problem {                                         /*Problem class: Constructs the problem class.*/ 
    constructor(lti, question, answers, type, image){    /*lti: The lti's for where the problem occurs*/
        this.lti = lti || [];                           /*question: the question text for the problem*/      
        this.question = question;                       /*answer: the answer(s) for the problem*/
        this.answers = answers || [];                     /*type: the type of problem it is, for now I have it dynamically selecting MC_NINPUT and MC_NCHOICE*/
        this.type = type;
        this.image = image;
    }
}

export function removeRow(e){                                                      /*When remove button is clicked, this function removes the choice row*/
    if(document.querySelectorAll(".choices").length > 2){
        e.target.parentNode.remove();
        if(document.querySelectorAll(".choices").length == 2){
            const removebtn = document.querySelectorAll(".remove");
            removebtn.forEach(input => input.disabled = true);
        }
    }
}

export function moveRow(e){                                                        /*When the up or down button is clicked, this function moves the row based on class name of the button*/
    const row = e.target.parentNode;
    const choiceList = document.getElementById("allChoices");
    if (e.target.className == "moveUp" && row.previousElementSibling != null) {
        choiceList.insertBefore(row, row.previousElementSibling);
    } 
    else if(e.target.className == "moveDown" && row.nextElementSibling != null){
        const nextRow = row.nextElementSibling;
        choiceList.insertBefore(row, nextRow.nextElementSibling);
    }
}

export function cancel(){                                                          //Returns to to home page of the problem creation tool
    window.location.href = "../";
}