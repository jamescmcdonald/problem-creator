import {Problem, removeRow, moveRow, cancel} from '../problemCreatorUtil.js';

class matchPairs extends Problem {                      /*matchPairs class: Extends the problems class with the choices and the match for that choices*/
    constructor(lti, question, answers, type, image, choices){
        super(lti, question, answers, type, image);
        this.choices = choices || [];
    }

    previewQuestion(){                                                 /*Allows the user to preview a question before submitting*/
        if(!document.querySelector(".matchForm").reportValidity()){
            return;
        }
        this.setProbData();
        this.buildPreview();
    }

    setProbData(){                                                     /*Sets the object of the problem*/
        this.image = null;
        this.lti = document.querySelector("#lti").value;
        this.question = document.querySelector("#question").value;
        this.type = "MATCH_PAIRS";
    
        document.querySelectorAll(".choices").forEach((current, i) => {
            this.choices.push(current.children[0].value);
            this.answers.push(current.children[1].value);
        },);
        console.log(this);
    }
    
    buildPreview(){                                                    //Builds the question being previewed
        document.querySelector("#prevProb").innerHTML = "";
        const preview = document.createElement("section");
        preview.innerHTML = "<h2>Preview Problem:</h2>";
        preview.setAttribute("id", "lineArea");
        const question = document.createElement("p");
        const table = document.createElement("table");
        const gradePrev = document.createElement("button");
        const isCorrect = document.createElement("h3");
        var choice = this.choices.slice();
        var answer = this.answers.slice(); 
        shuffleArray(choice);
        shuffleArray(answer);
        gradePrev.setAttribute("id", "gradePreview");
        gradePrev.innerText = "Go!";
        isCorrect.setAttribute("id", "isCorrect");
        question.innerText = this.question;
        preview.appendChild(question);

        var newLine = new previewDrawings;
        newLine.probData = this;

        choice.forEach(function(current, i) {
            var row = table.insertRow(i);
            var cell1 = row.insertCell(0);
            var divider = row.insertCell(1);
            var cell2 = row.insertCell(2);
            var cell1id = "col1" + i;
            var cell2id = "col2" + i;
            cell1.addEventListener("mousedown",  (event) => {
                newLine.startLine(event.target.parentNode);
            });
            cell1.addEventListener("mouseup",  (event) => {
                newLine.endLine(event.target.parentNode);
            });
            cell1.setAttribute("id", cell1id);
            cell2.setAttribute("id", cell2id);
            cell2.addEventListener("mousedown",  (event) => {
                newLine.startLine(event.target.parentNode);
            });
            cell2.addEventListener("mouseup",  (event) => {
                newLine.endLine(event.target.parentNode);
            });
            cell1.innerHTML = "<span>" + current +"</span>";
            cell2.innerHTML = "<span>" + answer[i] + "</span>";
            divider.innerText = "";
            cell1.style.textAlign = "right";
            cell2.style.textAlign = "left";
        },); 
        table.style.userSelect = "none";
        table.setAttribute("id", "answerTable")
        preview.appendChild(table);
        preview.appendChild(gradePrev);
        preview.appendChild(isCorrect);
        preview.addEventListener("mousemove",  (event) => {
            newLine.followLine(event);
        });
    
        document.querySelector("#prevProb").appendChild(preview);
        document.querySelector("#prevLast").disabled = false;
        document.querySelector("#gradePreview").disabled = true;
    }
    
    gradePreview(useranswers){                                        //Grades the question being previewed
        var isCorrect;
        for (let i = 0; i < useranswers.length; i++){
            var e1 = document.getElementById(useranswers[i][0]).innerText;
            var e2 = document.getElementById(useranswers[i][1]).innerText;
            var choice = this.choices.findIndex((choice) => { return choice == e1; });
            var answer = this.answers.findIndex((answer) => { return answer == e2; });
            if(choice === answer){
                isCorrect = "Correct!";
            }
            else{
                isCorrect = "Sorry, that's incorrect.";
                break;
            }
        }
        document.querySelector("#isCorrect").innerText = isCorrect;
    }
    
    submitMatch(){                                                     /*Creates the mcProblem object and then posts it to mcCreator.php which inserts the problem into the db*/
        if(!document.querySelector(".matchForm").reportValidity()){
            return;
        }
    
        this.setProbData();
    
        this.answers.forEach(function(current) {                      //URL encodes the reserved values
            current = encodeURIComponent(current);
        },);
        this.choices.forEach(function(current) {
            current = encodeURIComponent(current);
        },);
        this.question = encodeURIComponent(this.question);
    
        const options = {										                //options contains all the options for the post fetch call
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            body: 'pData='+JSON.stringify(this),
        }
    
        fetch('matchCreator.php', options)	                                    //Posts the data to the API endpoint and translates the results
            .then(response => {
                alert("Problem created successfully.");
                document.querySelector("#matchForm").reset(); 
                revert();
            })
            .catch(err => {
                console.error('Error: ', err);
            });
        document.querySelector("#prevLast").disabled = false;
    }

}
/*************************************************
PreviewDrawings holds all the matching data for the preview panel after it has been built,
this includes everything to do with the line drawings.
**************************************************/
class previewDrawings{
    constructor(inProgress, currentLine, e1, xCoord, yCoord, useranswers, probData){
        this.inProgress = inProgress;
        this.currentLine = currentLine;
        this.e1 = e1;
        this.xCoord = xCoord;
        this.yCoord = yCoord;
        this.useranswers = useranswers || []
        this.probData = probData;
    }

    startLine(e){                              //Starts the line drawing
        const line = document.createElement("hr");
        var coord = [];
        if (e.nodeName == "TR"){ return; }
        if(e.parentNode.children[0] != e){
            coord = this.getCoord(e, "level", false);
        }
        else{
            coord = this.getCoord(e, "level", true);
        }
        this.inProgress = true;
        this.currentLine = line;
        this.e1 = e;
        this.xCoord = coord[0];
        this.yCoord = coord[1];
    }
    
    followLine(e){                             //edits the line created in startLine to follow the cursor
        if (this.inProgress){
            const line = this.currentLine;
            document.body.onmouseup = () => {       //if mouse up occurs, delete the line. Logic for a permanent line is found in endLine()
                if(this.inProgress){
                    this.inProgress = false;
                    this.currentLine = undefined;
                    this.xCoord = undefined;
                    this.yCoord = undefined;
                    return line.remove();
                }
            };

            const x1 = this.xCoord;
            const y1 = this.yCoord;
            const x2 = e.pageX;
            const y2 = e.pageY;
            const length = Math.sqrt(((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1)));
            const cx = ((x1 + x2) / 2) - (length / 2);
            const cy = ((y1 + y2) / 2) - (1 / 2);
            const deg = Math.atan2((y1 - y2), (x1 - x2)) * (180 / Math.PI);
            line.style.left = cx + "px";
            line.style.top = cy + "px";
            line.style.width = length + "px";
            line.style.transform = "rotate(" + deg + "deg)";
            
            if(line.id != "drawingLine"){        //if not inserted, insert the dom element
                line.style.position = "absolute";
                line.style.border = "1px solid black";
                document.querySelector("#prevProb").children[0].appendChild(line);
                line.setAttribute("id", "drawingLine");
                this.currentLine = line;
            }
        }
    }
    
    endLine(e2){                                                           //Creates the permanent line drawing and answers
        var e1 = this.e1;
        if (e1 != undefined && e2.nodeName != "TR"){
            if (e1.offsetLeft != e2.offsetLeft){
                if(e1.parentNode.children[0] != e1){ 
                    const temp = e1;                                       //Sorts the elements inserted based on how the answers array has them inserted
                    e1 = e2;                                               //If e1 is in the first column then its correctly named, else it should be swapped with e2    
                    e2 = temp;
                }
                
                var el1, el2, level;
                if (e1.parentNode == e2.parentNode){
                    level = true;
                    el1 = this.getCoord(e1, "level", true);
                    el2 = this.getCoord(e2, "level", false);
                }
                else if (e1.offsetTop < e2.offsetTop){
                    el1 = this.getCoord(e1, "top", true);
                    el2 = this.getCoord(e2, "bottom", false);
                }
                else {
                    el1 = this.getCoord(e1, "bottom", true);
                    el2 = this.getCoord(e2, "top", false);
                }
                const x1 = el1[0];
                const y1 = el1[1];
                const x2 = el2[0];
                const y2 = (level ? y1 : el2[1]);
                const length = Math.sqrt(((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1)));
                const cx = ((x1 + x2) / 2) - (length / 2);
                const cy = ((y1 + y2) / 2) - (1 / 2);
                const deg = Math.atan2((y1 - y2), (x1 - x2)) * (180 / Math.PI);
                const line = document.createElement("hr");
                line.style.left = cx + "px";
                line.style.top = cy + "px";
                line.style.width = length + "px";
                line.style.transform = "rotate(" + deg + "deg)";
                line.style.position = "absolute";
                line.style.border = "1px solid black";
                var useranswers = this.useranswers;
                for(let i = this.useranswers.length - 1; i >= 0; i--){
                    if (useranswers[i][0] == e1.id || useranswers[i][1] == e2.id){
                        var deleteLine = useranswers[i][2];
                        document.getElementById(deleteLine).remove();
                        useranswers.splice(i, 1);
                    }
                }
                var lineID = "l" + e1.id;
                
                line.setAttribute("id", lineID);
                document.querySelector("#prevProb").children[0].appendChild(line);
                useranswers.push([e1.id, e2.id, lineID]);
                var probData = this.probData;
                if (useranswers.length == probData.answers.length){
                    document.querySelector("#gradePreview").disabled = false;
                    document.querySelector("#gradePreview").addEventListener("click", () => {probData.gradePreview(useranswers);});
                }
                this.useranswers = useranswers;
            }
            this.e1 = undefined;
        }
    }
    
    getCoord(element, lvl, col) {                  //Gets the coordinates for where the element is located, starts the line on the top left of the text
        var rect = element.children[0].getBoundingClientRect();
        var x = rect.left + window.pageXOffset;
        var y = rect.top + window.pageYOffset;
        if (col){                                           //if the element is in the left column, then the line should start on the right side of the text. Otherwise it should start on the left side
            x += element.children[0].offsetWidth;
        }
        if (lvl == "top"){                                  //If the element is above the row of the other element, then it should have the line start on the bottom corner
            y += 10;
        }
        else if (lvl == "level"){                           //Else if the element is in the same row as the other element, it should have the line start in the middle
            y += 3;
        }
        return [x, y];
    }
}

document.querySelectorAll('.moveUp').forEach(current => current.addEventListener('click', (event) => {
    moveRow(event);
}));
document.querySelectorAll('.moveDown').forEach(current => current.addEventListener('click', (event) => {
    moveRow(event);
}));
document.querySelectorAll('.remove').forEach(current => current.addEventListener('click', (event) => {
    removeRow(event);
}));
document.querySelector('#addRow').addEventListener('click', () => {
    mcAddRow();
});
document.querySelector('.cancelBtn').addEventListener('click', () => {
    cancel();
});
document.querySelector('.resetBtn').addEventListener('click', () => {
    revert();
});
document.querySelector('.previewBtn').addEventListener('click', () => {
    const matchProb = new matchPairs;
    matchProb.previewQuestion();
});
document.querySelector('.submitBtn').addEventListener('click', () => {
    const matchProb = new matchPairs;
    matchProb.submitMatch();
});
document.querySelector('#prevLast').addEventListener('click', () => {
    seePrevious();
});

function mcAddRow(){                                                        /*When add button is clicked, this function adds a choices row*/
    const newRow = document.querySelector(".choices").cloneNode(true);
    newRow.children[0].value = "";                                          /*CHANGED FOR MATCH prob*/
    newRow.children[1].value = "";
    newRow.children[2].addEventListener('click', (event) => {
        moveRow(event);
    });
    newRow.children[3].addEventListener('click', (event) => {
        moveRow(event);
    });
    newRow.children[4].addEventListener('click', (event) => {
        removeRow(event);
    });
    document.querySelector("#allChoices").appendChild(newRow);
    if(document.querySelectorAll(".choices").length == 3){
        const removebtn = document.querySelectorAll(".remove");
        removebtn.forEach(input => input.disabled = false);
    }
}

function revert(){                                                          /*When revert is clicked, this function reverts the choices rows back to the starting state of 2*/
    var count = document.getElementsByClassName("choices").length - 1;
    if(count > 1){
        while(count > 1){
            document.getElementsByClassName("choices")[count].remove();
            count--;
        }
    }
    if(document.querySelectorAll(".choices").length == 2){
        const removebtn = document.querySelectorAll(".remove");
        removebtn.forEach(input => input.disabled = true);
    }
    document.querySelector("#matchForm").reset()                            /*CHANGED FOR MATCH PROB*/
}
  
function seePrevious(){                                                     //Populates a table of previously created match problems
    
    const options = {										                /*options contains all the options for the post fetch call*/
		method: 'GET',
	}

    fetch('matchSelect.php', options)	                                    /*Posts the data to the API endpoint and translates the results*/
		.then(response => {
            return response.json();
		})
        .then(queryData =>{
            if(queryData[0].id != undefined){
                var queryQA = [];                                           //stores the question and answers data as a 3d array [question, [choice], [answer]]
                document.querySelector("#prevProb").innerHTML = "";
                const preview = document.createElement("section");
                preview.innerHTML = "<h2>Your previously submitted problems:</h2>";
                const table = document.createElement("table");
                table.setAttribute("class", "prevProb");
                for(let i = 0; i < queryData.length; i++){
                    var row = table.insertRow(i);
                    var cell1 = row.insertCell(0);
                    var cell2 = row.insertCell(1);
                    var cell3 = row.insertCell(2);
                    cell1.innerHTML = queryData[i].id;
                    cell2.innerHTML = queryData[i].lti;
                    cell3.innerHTML = queryData[i].problemtype;
                    queryQA.push([queryData[i].question, queryData[i].answer, queryData[i].choices]);
                    row.addEventListener('click', (event) => {insertPrevious(event, queryQA);});
                }
                var thead = table.createTHead();
                var row = thead.insertRow(0);
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                var cell3 = row.insertCell(2);
                cell1.innerHTML = "ID";
                cell2.innerHTML = "LTI";
                cell3.innerHTML = "Prob Type";
                preview.appendChild(table);
                document.querySelector("#prevProb").appendChild(preview);
            }
            else{
                document.querySelector("#prevProb").innerHTML = "<section><h2>You haven't submitted any questions yet.</h2></section>";
            }
            document.querySelector("#prevLast").disabled = true;
        })
		.catch(err => {
			console.error('Error: ', err);
		});
}

function insertPrevious(e, queryQA){                                                 /*Set up the problem from a problem that was created previously*/
    const row = e.target.parentNode;
    const rowNum = e.target.parentNode.rowIndex - 1;
    const image = null;
    const question = queryQA[rowNum][0];
    var atxt = queryQA[rowNum][1];
    atxt = atxt.replace("[", "");
    atxt = atxt.replace("]", "");
    atxt = atxt.replace(/"/g, "");
    var ctxt = queryQA[rowNum][2];
    ctxt = ctxt.replace("[", "");
    ctxt = ctxt.replace("]", "");
    ctxt = ctxt.replace(/"/g, "");
    const lti = row.cells[1].innerText;
    const answers = atxt.split(", ");
    const choices = ctxt.split(", ");
    const type = row.cells[2].innerText;

    var matchProblem = new matchPairs(lti, question, answers, type, image, choices);
    console.log(matchProblem);
    matchProblem.buildPreview();
}

function shuffleArray(arr) {                                              //Randomize positioning answers in preview pane
    var currentId = arr.length;
    var randomId;

    while (currentId != 0) {
        // Pick a remaining element...
        randomId = Math.floor(Math.random() * currentId);
        currentId--;
        // And swap it with the current element.
        [arr[currentId], arr[randomId]] = [arr[randomId], arr[currentId]];
    }

    return arr;
}