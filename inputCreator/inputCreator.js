class Problem {                                         /*Problem class: Constructs the problem class.*/ 
    constructor(lti, question, answer, type, image){    /*lti: The lti's for where the problem occurs*/
        this.lti = lti || [];                           /*question: the question text for the problem*/      
        this.question = question;                       /*answer: the answer(s) for the problem*/
        this.answer = answer || [];                     /*type: the type of problem it is, for now I have it dynamically selecting MC_NINPUT and MC_NCHOICE*/
        this.type = type;
        this.image = image;
    }
}