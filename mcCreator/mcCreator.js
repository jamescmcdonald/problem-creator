import {Problem, removeRow, moveRow, cancel} from '../problemCreatorUtil.js';

class MultipleChoiceProblem extends Problem {           /*MultipleChoiceProblems class: Extends the problems class with the choices that a MC problem could have*/
    constructor(lti, question, answers, type, image, choices, howManyAnswers){
        super(lti, question, answers, type, image);
        this.choices = choices || [];
        this.howManyAnswers = howManyAnswers;
    }

    previewQuestion(){                             /*Allows the user to preview a question before submitting*/
        if(!document.querySelector(".mcForm").reportValidity()){
            return;
        }
        this.setProbData();
        if (this.howManyAnswers <= 0){return}
        this.buildPreview();
    }

    setProbData(){                                        /*Sets the object of the problem*/
        var answers = [];
        var choices = [];
        this.image = null;
        this.lti = document.querySelector("#lti").value;
        this.question = document.querySelector("#question").value;
        const selectBox = document.querySelector("#pType").value;
    
        document.querySelectorAll(".choices").forEach(function(currentValue, i) {
                if (currentValue.children[0].checked){
                    answers.push(currentValue.children[1].value);

                }
                choices.push(currentValue.children[1].value);
            },);

        this.answers = answers
        this.choices = choices;
        this.howManyAnswers = answers.length;

        if(this.howManyAnswers == 0){
            alert("Must select at least one answers.");
            return;
        }
        
        this.type = this.determinePType(selectBox);
    
        console.log(this);
    }

    determinePType(selectBox){         /*Determines what the problem type should be, if choice box is liked, can make this function much simpler*/
        if (selectBox == "t/f" || selectBox == "yes/no"){
            return "MC_2CHOICE";
        }
        else{
            if (this.howManyAnswers == 1){return "MC_NCHOICE";}
            else{return "MC_NINPUT";}
        }
    }

    buildPreview(){                          /*Builds the question being previewed*/
        document.querySelector("#prevProb").innerHTML = "";
        const preview = document.createElement("section");
        preview.innerHTML = "<h2>Preview Problem:</h2>";
        const question = document.createElement("p");
        const table = document.createElement("table");
        const gradePrev = document.createElement("button");
        const isCorrect = document.createElement("h3");
        gradePrev.addEventListener("click", (event) => {this.gradePreview();});
        gradePrev.innerText = "Go!";
        isCorrect.setAttribute("id", "isCorrect");
        question.innerText = this.question;
        preview.appendChild(question);
        const choices = this.choices;
        const answers = this.answers;
        
        if (this.howManyAnswers > 1 ){
            choices.forEach((current, i) => {
                var row = table.insertRow(i);
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                var cell3 = row.insertCell(2);
                const check = document.createElement("input");
                check.setAttribute("type", "checkbox");
                check.setAttribute("class", "answerCheck");
                cell1.appendChild(check);
                cell2.innerHTML = current;
                if (answers.includes(current)){cell3.innerHTML = "(Correct)"}
            },);
        } 
        else{
            choices.forEach((current, i) => {
                var row = table.insertRow(i);
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                var cell3 = row.insertCell(2);
                const check = document.createElement("input");
                check.setAttribute("type", "radio");
                check.setAttribute("class", "answerCheck");
                check.setAttribute("name", "answerCheck");
                cell1.appendChild(check);
                cell2.innerHTML = current;
                if (answers.includes(current)){cell3.innerHTML = "(Correct)";}
            },);
        }
    
        preview.appendChild(table);
        preview.appendChild(gradePrev);
        preview.appendChild(isCorrect);
        document.querySelector("#prevProb").appendChild(preview);
        document.querySelector("#prevLast").disabled = false;
    }

    gradePreview(){                                        /*Grades the question being previewed*/
        const answerChoices = document.querySelectorAll(".answerCheck");
        var isCorrect;
        for(let i = 0; i < answerChoices.length; i++){
            if((answerChoices[i].checked == true && this.answers.includes(this.choices[i]) == true) || 
                (answerChoices[i].checked == false && this.answers.includes(this.choices[i]) == false)){
                isCorrect = "Correct!";
            }
            else{
                isCorrect = "Sorry, that's incorrect.";
                break;
            }
        }
        document.querySelector("#isCorrect").innerText = isCorrect;
    }

    submitMC(){                                        /*Creates the mcProblem object and then posts it to mcCreator.php which inserts the problem into the db*/
        if(!document.querySelector(".mcForm").reportValidity()){
            return;
        }
        this.setProbData();
        if (this.howManyAnswers == -1){
            return;
        }
    
        this.choices.forEach((current) => {        /*URL encodes the reserved values*/
            current = encodeURIComponent(current);
        },);
        this.answers.forEach((current) => {        
            current = encodeURIComponent(current);
        },);
        this.question = encodeURIComponent(this.question);
    
        const options = {										/*options contains all the options for the post fetch call*/
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            body: 'pData='+JSON.stringify(this),
        }
    
        fetch('mcCreator.php', options)	                        /*Posts the data to the API endpoint and translates the results*/
            .then(response => {
                alert("Problem created successfully.");
                document.querySelector("#mcForm").reset(); 
                revert();
            })
            .catch(err => {
                console.error('Error: ', err);
            });
        document.querySelector("#prevLast").disabled = false;
    }
}

document.querySelectorAll('.moveUp').forEach((current) => current.addEventListener('click', (event) => {
    moveRow(event);
}));
document.querySelectorAll('.moveDown').forEach((current) => current.addEventListener('click', (event) => {
    moveRow(event);
}));
document.querySelectorAll('.remove').forEach((current) => current.addEventListener('click', (event) => {
    removeRow(event);
}));
document.querySelector('#addRow').addEventListener('click', (event) => {
    mcAddRow();
});
document.querySelector('.cancelBtn').addEventListener('click', (event) => {
    cancel();
});
document.querySelector('.resetBtn').addEventListener('click', (event) => {
    revert();
});
document.querySelector('.previewBtn').addEventListener('click', (event) => {
    var mcProb = new MultipleChoiceProblem;
    mcProb.previewQuestion();
});
document.querySelector('.submitBtn').addEventListener('click', (event) => {
    var mcProb = new MultipleChoiceProblem;
    mcProb.submitMC();
});
document.querySelector('#prevLast').addEventListener('click', (event) => {
    seePrevious();
});
document.querySelector('#pType').addEventListener('change', (event) => {
    setText();
});

function mcAddRow(){                                    /*When add button is clicked, this function adds a choice row*/
    const newRow = document.querySelector(".choices").cloneNode(true);
    newRow.children[0].checked = false;
    newRow.children[1].value = "";
    newRow.children[2].addEventListener('click', (event) => {
        moveRow(event);
    });
    newRow.children[3].addEventListener('click', (event) => {
        moveRow(event);
    });
    newRow.children[4].addEventListener('click', (event) => {
        removeRow(event);
    });
    document.querySelector("#allChoices").appendChild(newRow);
    if(document.querySelectorAll(".choices").length == 3){
        const removebtn = document.querySelectorAll(".remove");
        removebtn.forEach(input => input.disabled = false);
    }
}

function revert(){                                      /*When revert is clicked, this function reverts the choice rows back to the starting state of 2*/
    var count = document.getElementsByClassName("choices").length - 1;
    if(count > 1){
        while(count > 1){
            document.getElementsByClassName("choices")[count].remove();
            count--;
        }
    }
    if(document.querySelectorAll(".choices").length == 2){
        const removebtn = document.querySelectorAll(".remove");
        removebtn.forEach(input => input.disabled = true);
    }
    setMcText();
}

function setMcText(){                                   /*Sets the text for the mc selection*/
    document.querySelectorAll(".answerChoice")[0].removeAttribute("readonly");
    document.querySelectorAll(".answerChoice")[1].removeAttribute("readonly");
    document.querySelectorAll(".answerChoice")[0].value = "";
    document.querySelectorAll(".answerChoice")[1].value = "";
    document.querySelectorAll(".correctAnswer")[0].setAttribute("type", "checkbox");
    document.querySelectorAll(".correctAnswer")[1].setAttribute("type", "checkbox");
    document.querySelector("#addRow").disabled = false;
}

function setText(){                                                 /*Changes the form based on the selected type, if t/f or yes/no, then the input boxes get locked, and the add row button gets disabled.*/
    const choice1 = document.querySelectorAll(".answerChoice")[0];  /*This helps with potential fat fingered t/f or yes/no choices.*/
    const choice2 = document.querySelectorAll(".answerChoice")[1];
    const correct = document.querySelectorAll(".correctAnswer");
    
    if(document.getElementsByClassName("choices").length > 2){      
        revert();
    }

    switch (document.querySelector("#pType").value){
        case "mcQuestion":
            setMcText();
            break;
        case "t/f":
            choice1.value = "True";
            choice2.value = "False";
            choice1.setAttribute("readonly", true);
            choice2.setAttribute("readonly", true);
            correct[0].setAttribute("type", "radio");
            correct[1].setAttribute("type", "radio");
            document.querySelector("#addRow").disabled = true;
            break;
        case "yes/no":
            choice1.value = "Yes";
            choice2.value = "No";
            choice1.setAttribute("readonly", true);
            choice2.setAttribute("readonly", true);
            correct[0].setAttribute("type", "radio");
            correct[1].setAttribute("type", "radio");
            document.querySelector("#addRow").disabled = true;
            break;
    }
}

function seePrevious(){
    const options = {										/*options contains all the options for the post fetch call*/
		method: 'GET',
	}

    fetch('mcSelect.php', options)	                        /*Posts the data to the API endpoint and translates the results*/
		.then(response => {
            return response.json();
		})
        .then(queryData =>{
            if(queryData[0].id != undefined){
                var queryQA = [];                           //stores the question and answers data as a 3d array [id][question, answers, choices]
                document.querySelector("#prevProb").innerHTML = "";
                const preview = document.createElement("section");
                preview.innerHTML = "<h2>Your previously submitted problems:</h2>";
                const table = document.createElement("table");
                table.setAttribute("class", "prevProb");
                for(let i = 0; i < queryData.length; i++){
                    var row = table.insertRow(i);
                    var cell1 = row.insertCell(0);
                    var cell2 = row.insertCell(1);
                    var cell3 = row.insertCell(2);
                    cell1.innerHTML = queryData[i].id;
                    cell2.innerHTML = queryData[i].lti;
                    cell3.innerHTML = queryData[i].problemtype;
                    queryQA.push([queryData[i].question, queryData[i].answer, queryData[i].choices]);
                    row.addEventListener('click', event => {insertPrevious(event, queryQA)});
                }
                var thead = table.createTHead();
                var row = thead.insertRow(0);
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                var cell3 = row.insertCell(2);
                cell1.innerHTML = "ID";
                cell2.innerHTML = "LTI";
                cell3.innerHTML = "Prob Type";
                preview.appendChild(table);
                document.querySelector("#prevProb").appendChild(preview);
            }
            else{
                document.querySelector("#prevProblem").innerHTML = "<section><h2>You haven't submitted any questions yet.</h2></section>";
            }
            document.querySelector("#prevLast").disabled = true;
        })
		.catch(err => {
			console.error('Error: ', err);
		});
}

function insertPrevious(e, queryQA){                        /*Set up the problem from a problem that was created previously*/
    const row = e.target.parentNode;
    const rowNum = e.target.parentNode.rowIndex - 1;
    const image = null;
    const question = queryQA[rowNum][0];
    var atxt = queryQA[rowNum][1];
    atxt = atxt.replace("[", "");
    atxt = atxt.replace("]", "");
    atxt = atxt.replace(/"/g, "");
    var ctxt = queryQA[rowNum][2];
    ctxt = ctxt.replace("[", "");
    ctxt = ctxt.replace("]", "");
    ctxt = ctxt.replace(/"/g, "");
    const lti = row.cells[1].innerText;
    const answers = atxt.split(", ");
    const choices = ctxt.split(", ");
    const type = row.cells[2].innerText;
    const howManyAnswers = answers.length;

    var mcProb = new MultipleChoiceProblem(lti, question, answers, type, image, choices, howManyAnswers);
    console.log(mcProb);
    mcProb.buildPreview();
}